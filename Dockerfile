FROM php:8.0-fpm-alpine3.14

# Copy the php config file
COPY ./docker/php/php-fpm.conf /usr/local/etc/php-fpm.d/www.conf

RUN cd /tmp && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php  --quiet --install-dir="/usr/local/bin" --filename="composer" --2 && \
    php -r "unlink('composer-setup.php');"

RUN apk --update add zlib-dev libpng-dev ca-certificates

RUN docker-php-ext-install pdo_mysql gd opcache

COPY ./localhost.crt /usr/local/share/ca-certificates/localhost.crt

RUN update-ca-certificates

RUN wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/latest/download/drush.phar && \
    mv drush.phar /usr/local/bin/drush && \
    chmod +x /usr/local/bin/drush

USER www-data

# Copy the application code
COPY . /app

VOLUME ["/app"]
WORKDIR /app
