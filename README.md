# Drupal 9

- Docker Drupal: https://github.com/wodby/docker4drupal

- L'api Drupal: https://api.drupal.org/api/drupal

- Drupal Coding Standard: https://www.drupal.org/docs/develop/standards/ coding-standards

- Deprecate code check: https://github.com/mglaman/drupal-check

- VSCode Configuration: https://www.drupal.org/docs/develop/development-tools/configuring-visual-studio-code

- Route configuration: https://www.drupal.org/docs/8/api/routing-system/structure-of-routes

- Github source code: https://github.com/PacktPublishing/Drupal-9-Module-Development-Third-Edition

- Route with param node:

```yaml
  path: '/hello/{param}'
  options:
   parameters:
     param:
       type: entity:node
 ```

We can do one better. If, instead of {param}, we name the parameter {node} (the machine name of the entity type), we can avoid having to write the parameters option in the route completely.

- How to disable cache for dev mode: https://www.drupal.org/node/2598914

- Use the StringTranslationTrait in order to expose the translation function.

- Statically, you would use the global \Drupal class to instantiate a service:

```php
$service = \Drupal::service('hello_world.salutation');
```

This is how we use services in .module files and classes that are not exposed the service container;

- Drupal Form API reference page (https://api.drupal.org/api/drupal/ elements/9.0.x)

- Form validation error messages, by default, are printed at the top of the page. However, with the core Inline Form Errors module, we can have the form errors printed right beneath the actual elements.

- Drupal 8 Messenger Service used to show  messages to the user.

- The form builder can be injected using the form_builder service key or used statically via the shorthand:

```php
$builder = \Drupal::formBuilder();
$form = $builder->getForm('Drupal\hello_world\Form\SalutationConfigurationForm');
```

- logger: ./drush watchdog:show ou ./drush watchdog:list

- Entity Structure

```php
<?php

// Add code here showing extending from the entity class, and then Nodes extending from the ContentEntity class

// Base Entity Class
abstract class Entity implements EntityInterface {
    // Code
}

// Content Entity
abstract class ContentEntityBase extends Entity implements \IteratorAggregate, ContentEntityInterface, TranslationStatusInterface {
    // Code
}

class Node extends ContentEntityBase implements NodeInterface {
    // Code
}

abstract class ConfigEntityBase extends Entity implements ConfigEntityInterface {
    //
}

// Configuration Entity
class Block extends ConfigEntityBase implements BlockInterface, EntityWithPluginCollectionInterface {
    // Code
}
```
