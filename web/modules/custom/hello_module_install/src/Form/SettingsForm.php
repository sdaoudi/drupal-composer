<?php

namespace Drupal\hello_module_install\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Psr\Container\ContainerInterface;

/**
 * Provides a Hello module install settings form.
 */
class SettingsForm extends ConfigFormBase {

  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

    /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hello_module_install.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hello_module_install_settings';
  }

  /**
   * Example: https://gist.github.com/WengerK/5c7f3df9aa5c27a95cd8a29b72b28a19
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hello_module_install.settings');
    $form['my_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('My label'),
      '#required' => TRUE,
      '#default_value' => $config->get('my_label'),
    ];
    $form['my_boolean'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('My bool'),
      '#required' => FALSE,
      '#default_value' => $config->get('my_boolean'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->config('hello_module_install.settings')
      ->set('my_label', $form_state->getValue('my_label'))
      ->set('my_boolean', $form_state->getValue('my_boolean'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('The message has been sent.'));
    // $form_state->setRedirect('<front>');
  }

}
