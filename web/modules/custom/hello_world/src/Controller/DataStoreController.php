<?php

namespace Drupal\hello_world\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\node\Entity\Node;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Data store routes.
 */
class DataStoreController extends ControllerBase {
  public function __construct(
    private PrivateTempStoreFactory $temp_store_factory,
    private SharedTempStoreFactory $sharedTempStoreFactory,
    private UserDataInterface $userData,
    private AccountProxyInterface $accountProxy,
    ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('tempstore.shared'),
      $container->get('user.data'),
      $container->get('current_user'),
    );
  }

  /**
   * Builds the response.
   */
  public function tempStore() {
    /*
    $store = $this->tempStoreFactory->get('hello_world.my_collection');
    $store->set('my_private_key', 'my_private_value');
    $value = $store->get('my_private_key');
    */

    $store = $this->sharedTempStoreFactory->get('hello_world.my_collection');
    $metadata = $store->getMetadata('my_shared_key');
    // dump($metadata);
    //$store->set('my_shared_key', 'my_shared_value2');
    $value = $store->get('my_shared_key');

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works! '. $value),
    ];

    return $build;
  }

  public function userData()
  {
    $current_user_id = $this->accountProxy->id();
    //$this->userData->set('hello_world', $current_user_id, 'user_data_key_name', 'user_data_value');

    $value = $this->userData->get('hello_world', $current_user_id, 'user_data_key_name');
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works! '. $value),
    ];

    return $build;
  }

  public function customSalutationMessage(Request $request)
  {
    $message = $request->query->get('message');


    if (NULL !== $message) {
      $read_only_config = $this->config('hello_world.custom_salutation');
      $read_and_write_config = $this->configFactory->getEditable('hello_world.custom_salutation');
      $read_and_write_config->set('salutation', $message);
      $read_and_write_config->save();
      $this->messenger()->addMessage("message '$message' is saved with sucess");
      // dd($read_only_config->get('salutation'));
    }

    return $this->redirect('<front>');
  }

  // http://slides.com/chadpeppers/deck-2#/12
  public function entityManagerRoute()
  {
    // create new node
    // $info = array('type' => 'landing_page', 'title' => 'My Auto Landing page');
    // $node = $this->entityTypeManager()->getStorage('node')->create($info);
    // $node->save();


    $storage = $this->entityTypeManager()->getStorage('node');
    /** @var Node */
    $node = $storage->load(1);

    $languageManager = $this->languageManager();
    $langCode = $languageManager->getCurrentLanguage()->getId();
    if ($node->hasTranslation($langCode)) {
      $trans = $node->getTranslation($langCode);
      $title = $trans->getTitle();

      dump($trans->get('body')->getString());
      dump($title);
      $url = $trans->toUrl('canonical')->toString();
      dump($url);
    }

    // $node->set('title', 'custom title');
    // $node->save();
    // $nodes = $storage->loadMultiple(array(1, 2, 3));
    // foreach ($nodes as $node) {
    //   dump($node->label());
    // }

    // $nid = 1;
    // $node = Node::load($nid);
    // $node->set('title', 'My Awesome New Title');
    // $node->set('field_my_field', 'My New field');
    // $node->save();

    die('fin');
  }

}
