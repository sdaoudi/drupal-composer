<?php

namespace Drupal\hello_world\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGenerator;
use Drupal\hello_world\Form\SalutationConfigurationForm;
use Drupal\hello_world\HelloWorldSalutation;
use Psr\Container\ContainerInterface;

class HelloWorldController extends ControllerBase
{

  /**
   * HelloWorldController constructor.
   *
   * @param \Drupal\hello_world\HelloWorldSalutation $salutation
   */
  public function __construct(
    private HelloWorldSalutation $salutation,
    private FormBuilderInterface $builder,
    private LinkGenerator $linkGenerator,
    private LoggerChannelFactory $logger,
    private LoggerChannel $loggerChannel,
    private StateInterface $state,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hello_world.salutation'),
      $container->get('form_builder'),
      $container->get('link_generator'),
      $container->get('logger.factory'),
      $container->get('hello_world.logger.channel.hello_world'),
      $container->get('state'),
    );
  }

/**
   * Hello World.
   *
   * @return array
   *   Our message.
   */
  public function helloWorld() {
    $url = Url::fromRoute('hello_world.hello_settings');
    $link = $this->linkGenerator->generate($this->t('Go to settings'), $url);

    return [
      '#markup' => $this->salutation->getSalutation().'<br>'.$link,
    ];
  }

  public function helloRender() {
    return $this->salutation->getSalutationComponent();
  }

  public function helloSettings() {
    $form = $this->builder->getForm(SalutationConfigurationForm::class);

    return $form;
  }

  public function helloLog()
  {
    $this->logger->get('hello_world')->error('This is my error message');
    $this->loggerChannel->error('error with logger channel -> '.$this->state->get('hello_world.foo'));

    return [
      '#markup' => $this->t('Logging done'),
    ];
  }
}
