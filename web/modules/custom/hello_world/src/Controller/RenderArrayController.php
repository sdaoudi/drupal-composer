<?php

namespace Drupal\hello_world\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

class RenderArrayController extends ControllerBase
{
  public function markup()
  {
    return [
      '#markup' => '<h1>Hello World</h1>'
    ];
  }

  public function plainText()
  {
    return [
      '#plain_text' => '<h1>Hello World</h1>'
    ];
  }
  public function htmlTag()
  {
    // Drupal\Core\Render\Element\HtmlTag
    return [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Hello World Example',
    ];
  }

  public function container()
  {
    $build = [];

    $build['#type'] = 'container';

    $build['steve'] = [
      '#type' => 'html_tag',
      '#tag' => 'h1',
      '#attributes' => [
        'class' => ['blue'],
      ],
      '#value' => 'Nested Arrays',
    ];

    $build['content'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'Some new text',
      '#attributes' => [
        'id' => 'user',
        'title' => 'title',
        'class' => ['some-class1', 'class2'],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'The current time is @time',
        '#attached' => [
          'placeholders' => [
            '@time' => [
              '#plain_text' => date('h:i:s A'),
            ]
          ]
        ]
      ],
    ];

    $build['footer'] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'This the Footer 2',
        '#weight' => 2
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'This the Footer 1',
        '#weight' => 1
      ],
    ];
    $build['#attached'] = [
      'library' => [
        'hello_world/my-library',
      ],
    ];
    // $build['#cache'] = [
    //   'max-age' => 0,
    // ];

    return $build;
  }

  public function theme()
  {
    return [
      '#theme' => 'simple_theme_example',
      '#header' => 'Simple theme example',
      '#body' => 'Quick description',
      '#repeat' => 4,
    ];
  }

  public function itemList()
  {
    $build = [];
    $items = [
      'Item 1',
      'Item 2'
    ];

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items
    ];

    $links = [
      [
        'title' => 'Link 1',
        'url' => Url::fromRoute('<front>'),
      ],
      [
        'title' => 'Link 2',
        'url' => Url::fromRoute('hello_world.hello'),
      ]
    ];

    $build['links'] = [
      '#theme' => 'links',
      '#links' => $links,
      '#set_active_class' => true,
    ];

    $header = ['Column 1', 'Column 2'];
    $rows = [
      ['Row 1, Column 1', 'Row 1, Column 2'],
      ['Row 2, Column 1', 'Row 2, Column 2']
    ];
    $build['table'] =  [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }
}
