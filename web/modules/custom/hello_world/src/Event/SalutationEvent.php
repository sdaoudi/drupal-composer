<?php

namespace Drupal\hello_world\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event class to be dispatched from the HelloWorldSalutation service.
 */
class SalutationEvent extends Event
{
  const EVENT = 'hello_world.salutation_event';

  /**
   * The salutation message.
   */
  protected string $message;

  public function getValue(): string {
    return $this->message;
  }

  public function setValue(string $message) {
    $this->message = $message;
  }
}
