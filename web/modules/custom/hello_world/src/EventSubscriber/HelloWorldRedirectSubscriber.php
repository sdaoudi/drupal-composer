<?php

namespace Drupal\hello_world\EventSubscriber;

use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\hello_world\Event\SalutationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class HelloWorldRedirectSubscriber implements EventSubscriberInterface
{
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected CurrentRouteMatch $currentRouteMatch,
    protected Messenger $messenger
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'][] = ['onRequest', 0];
    $events[SalutationEvent::EVENT][] = ['onSalutation', 0];
    return $events;
  }

  /**
   * Handler for the kernel request event.
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    $path = $request->getPathInfo();
    if ('hello_world.hello' !== $this->currentRouteMatch->getRouteName()) {
      return;
    }

    $roles = $this->currentUser->getRoles();
    //'sdaoudi' === $this->currentUser->getAccount()->getAccountName()
    if (in_array('non_grata', $roles)) {
      $event->setResponse(new RedirectResponse('/'));
    }
  }

  public function onSalutation(SalutationEvent $event) {
    //dd($event);
    //$this->messenger->addMessage('message called: '.$event->getValue());
  }
}
