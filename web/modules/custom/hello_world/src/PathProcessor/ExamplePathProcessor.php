<?php

namespace Drupal\hello_world\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

class ExamplePathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface
{
  /**
  * {@inheritdoc}
  */
  public function processInbound($path, Request $request) {
    // $path = str_replace('/prefix', '', $path);
    // $path = !empty($path) ? $path : '/node';
    return $path;
  }

  /**
  * {@inheritdoc}
  */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // $path = '/prefix'.$path;
    return $path;
  }
}
