<?php

namespace Drupal\hello_world\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGenerator;
use Drupal\hello_world\HelloWorldSalutation;
use Psr\Container\ContainerInterface;

/**
* Hello World Salutation block.
*
* @Block(
*  id = "hello_world_salutation_block",
*  admin_label = @Translation("Hello world salutation"),
* )
*/
class HelloWorldSalutationBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * Constructs a HelloWorldSalutationBlock.
   */
  public function __construct(
    array $configuration, $plugin_id, $plugin_definition,
    protected HelloWorldSalutation $salutation,
    protected LinkGenerator $linkGenerator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hello_world.salutation'),
      $container->get('link_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    if ($config['enabled'] === 0) {
      return [];
    }


    $url = Url::fromRoute('hello_world.hello');
    $link = $this->linkGenerator->generate($this->t('Go to hello page'), $url);

    return [
      '#markup' => $this->salutation->getSalutation().'<br>'.$link,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'enabled' => 1,
    ];
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this box if you want to enable this feature.'),
      '#default_value' => $config['enabled'],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['enabled'] = $form_state->getValue('enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    // dd($form_state);
    // Like validateForm
  }
}
