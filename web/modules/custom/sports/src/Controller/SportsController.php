<?php

namespace Drupal\sports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Sports routes.
 */
class SportsController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $this->connection->insert('players');
    $fields = ['name' => 'Diego M', 'data' => serialize(['known for' => 'Hand of God'])];
    $id = $this->connection->insert('players')->fields($fields)->execute();

    $result = $this->connection->query("SELECT * FROM {players} WHERE [id]= :id", [':id' => $id]);
    dd($result->fetch());
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
