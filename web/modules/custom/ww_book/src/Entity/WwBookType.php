<?php

namespace Drupal\ww_book\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Ww book type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "ww_book_type",
 *   label = @Translation("Ww book type"),
 *   label_collection = @Translation("Ww book types"),
 *   label_singular = @Translation("ww book type"),
 *   label_plural = @Translation("ww books types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ww books type",
 *     plural = "@count ww books types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\ww_book\Form\WwBookTypeForm",
 *       "edit" = "Drupal\ww_book\Form\WwBookTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\ww_book\WwBookTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer ww book types",
 *   bundle_of = "ww_book",
 *   config_prefix = "ww_book_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/ww_book_types/add",
 *     "edit-form" = "/admin/structure/ww_book_types/manage/{ww_book_type}",
 *     "delete-form" = "/admin/structure/ww_book_types/manage/{ww_book_type}/delete",
 *     "collection" = "/admin/structure/ww_book_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class WwBookType extends ConfigEntityBundleBase {

  /**
   * The machine name of this ww book type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the ww book type.
   *
   * @var string
   */
  protected $label;

}
