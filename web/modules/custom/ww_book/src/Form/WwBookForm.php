<?php

namespace Drupal\ww_book\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the ww book entity edit forms.
 */
class WwBookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New ww book %label has been created.', $message_arguments));
        $this->logger('ww_book')->notice('Created new ww book %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The ww book %label has been updated.', $message_arguments));
        $this->logger('ww_book')->notice('Updated ww book %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.ww_book.canonical', ['ww_book' => $entity->id()]);

    return $result;
  }

}
