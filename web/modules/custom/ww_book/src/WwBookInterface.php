<?php

namespace Drupal\ww_book;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a ww book entity type.
 */
interface WwBookInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
